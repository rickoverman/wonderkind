<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
    <header class="row">
        @include('includes.header')
    </header>
    <div class="container">

        <div id="main" class="row">

            <!-- main content -->
            <div id="content" class="col-md-8">
                @yield('content')
            </div>

        </div>

    </div>
    <footer class="row page-footer">
        @include('includes.footer')
    </footer>
</body>
</html>