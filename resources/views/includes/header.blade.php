<nav>
    <div class="nav-wrapper">
        <a href="#" class="left brand-logo"><img width="200" src="{{ asset('images/logo.png') }}"></a>
        <ul id="nav-mobile" class="right">
            <li><a href="{{ url('/calculator') }}">Calculator</a></li>
            <li><a href="{{ url('/about') }}">About</a></li>
        </ul>
    </div>
</nav>
