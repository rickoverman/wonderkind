
<footer class="page-footer">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Tweet Reach Calculator</h5>
                <p class="grey-text text-lighten-4">
                    This Laravel application calculates the reach of a specific tweet.
                </p>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2018 Copyright Rick Overman
        </div>
    </div>
</footer>