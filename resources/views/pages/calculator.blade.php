@extends('layouts.default')
@section('content')

    <h1>Calculator</h1>

    <div class="row">
        <form class="col s12" method="POST" action="">
            {{ csrf_field() }}
            <div class="row">
                <div class="input-field col s9">
                    <input placeholder="https://twitter.com/ricky_overman/status/955245490681139200" id="tweet_url" name="tweet_url" type="text" value="https://twitter.com/manoloidee/status/955137757436895239" class="validate">
                    <label for="tweet_url">Tweet URL</label>
                </div>
                <div class="input-field col s3">
                    <submit name="submit" value="Calculate">
                </div>
            </div>
        </form>
    </div>

    @if ($reach)
        <div class="row">

            <div class="col s3">
                <a class="btn btn-floating pulse">{{ $reach->users }}</a> Users
            </div>

            <div class="col s9">
                <a class="btn btn-floating pulse red" style="width:100px;height:100px;padding-top:30px;">{{ $reach->followers }}</a> <b>Followers</b><br><br>
            </div>
        </div>
    @endif



@stop