@extends('layouts.default')
@section('content')
    <h1 >Tweet Reach Calculator</h1>
    <p>
        This Laravel application calculates the reach of a specific tweet. A user is able to enter the URL of a tweet and the application will lookup the people who retweeted the tweet using the Twitter API. The application then sums up the amount of followers each user has that has retweeted the tweet. These results are stored in the cache for two hours. If the user tries to calculate the reach of a tweet that has already been calculated the results are returned from the cache. After two hours the cache should will be updated.
    </p>
@stop