<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReachesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reaches', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('tweet_id');
            $table->bigInteger('followers');
            $table->bigInteger('users');
            $table->longText('text')->nullable();
            $table->string('media_url_https')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reaches');
    }
}
