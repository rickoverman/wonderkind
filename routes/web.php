<?php

use Illuminate\Http\Request;
use Library\TweetReach;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('calculator');
});


/**
 * Wonderkind page routes
 */

Route::any('/calculator', function (Request $request) {

    $tweet_url = $request->input('tweet_url');
    $reach = TweetReach::getReachData($tweet_url);

    return view('pages/calculator', ['reach' => $reach]);

});


Route::get('/about', function () {
    return view('pages/about');
});


use App\Reach;
Route::get('/test', function () {

    $tweetID = 955137757436895239;
    $reach = Reach::where('tweet_id',$tweetID)->first();

    echo strtotime(date('Y-m-d h:i:s',strtotime($reach['created_at'])));
    echo "<br>";
    echo  strtotime('-2 hours');

    echo "<br><br>";

    echo strtotime($reach['created_at']);
    echo "<br>";
    echo date('Y-m-d h:i:s',strtotime('-1 hours'));

    if(isset($reach['created_at']) && strtotime($reach['created_at']) < strtotime('-2 hours')){
        return 'a';
    }else{
        return 'b';
    }


});
