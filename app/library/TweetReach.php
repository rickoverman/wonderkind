<?php

namespace Library;

use App\Reach;


class TweetReach {

    static function getReachData($url) {

        $url = trim($url);
        if(!$url) return false;

        $r = parse_url($url);
        $tweetID = (int) substr($r['path'], strrpos($r['path'], '/')+1);

        $reach = Reach::where('tweet_id',$tweetID)->first();
        if(isset($reach['created_at']) && strtotime($reach['created_at']) < strtotime('-2 hours')){
            $reach->forceDelete();
        }


        try{
            $reach = Reach::where('tweet_id',$tweetID)->first();
            if(!$reach)
            {

                $followers_count = 0;
                $rters = \Twitter::getRters(['id'=>$tweetID]);
                $users_count = count($rters->ids);

                if($users_count){

                    foreach($rters->ids as $rtid) {
                        $user = \Twitter::getUsers(['user_id' => $rtid, 'format' => 'array']);
                        $followers_count = $followers_count + $user['followers_count'];
                    }

                }

                $reach = new Reach;

                $reach->tweet_id = $tweetID;
                $reach->followers = $followers_count;
                $reach->users = $users_count;

                $reach->save();

            }

        }catch(\Exception $e){

            $reach = Reach::where('tweet_id',$tweetID)->first();

        }


        return $reach;

    }




}
